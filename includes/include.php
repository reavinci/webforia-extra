<?php
require_once WEBFORIA_EXTRA_TEMPLATE . 'includes/metabox/metabox.php';
require_once WEBFORIA_EXTRA_TEMPLATE . 'includes/widget/widget.php';

if(rt_var('dev')){
    require_once WEBFORIA_EXTRA_TEMPLATE . 'includes/white-label/white-label.php';
}
