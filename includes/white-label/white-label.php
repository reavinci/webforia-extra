<?php

/*=================================================
* LOGIN - CHANGE LOGO
=================================================== */
function wex_login_logo() {
	$logo = apply_filters('wwl_login_logo', WEBFORIA_EXTRA_ASSETS . '/includes/white-label/assets/webforia-logo.png !important; }');

    echo '<style type="text/css">h1 a { background-image:url('.$logo.'}</style>';
}
add_action( 'login_head', 'wex_login_logo' );


/*=================================================
* LOGIN - URL LOGO
=================================================== */
function wex_login_logo_url() {
    return 'https://webforia.id/';
}
add_filter( 'login_headerurl', 'wex_login_logo_url' );

/*=================================================
* LOGIN - TITLE
=================================================== */
function wex_login_logo_url_title() {
    return 'Webforia Studio';
}
add_filter( 'login_headertitle', 'wex_login_logo_url_title' );

/*=================================================
* LOGIN - STYLE REGISTER
=================================================== */
function wex_login_stylesheet() {
    wp_enqueue_style( 'retheme-login', WEBFORIA_EXTRA_ASSETS . '/includes/white-label/assets/login.css' );
}
add_action( 'login_enqueue_scripts', 'wex_login_stylesheet' );

/*=================================================
* ADMIN - REMOVE MENU LINK
=================================================== */
function wex_login_remove_link(){
	wp_enqueue_style('webforia-white-label', WEBFORIA_EXTRA_ASSETS . '/includes/white-label/assets/admin.css', '1.0.0');
}
add_action('admin_enqueue_scripts', 'wex_login_remove_link');

