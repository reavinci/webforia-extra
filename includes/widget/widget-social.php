<?php
namespace Retheme\Widget;

class Widget_Sosmed extends \WP_Widget {

	public function __construct() {
		$args = array(
			'classname' => 'retheme-widget-sosmed',
		);

		parent::__construct('retheme_widget_sosmed', 'Webforia - Social Link', $args);
	}

	// VIEW
	public function widget($args, $instance) {

		// outputs the content of the widget
		if (!isset($args['widget_id'])) {
			$args['widget_id'] = $this->id;
		}

		// widget ID with prefix for use in ACF API functions
		$widget_id = 'widget_' . $args['widget_id'];


		$title = rt_get_field('title', $widget_id);

		echo $args['before_widget'];

		if ($title) {
			echo $args['before_title'] . esc_html($title) . $args['after_title'];
		}


		$title = rt_get_field("title", $widget_id);
		$style = rt_get_field("style", $widget_id);
		$layout = rt_get_field("layout", $widget_id);
		

		$classes[] = "rt-socmed";
		$classes[] = "rt-socmed--{$style}";
		$classes[] = "rt-socmed--{$layout}";


		$data['layout'] = $layout;
		$data['class'] = implode(" ",$classes);

		wex_get_template_part('widget/social', $data);

		echo $args['after_widget'];
	}

	// BACKEND
	public function form($instance) {

		
	}

	// UPDATE
	public function update($new_instance, $old_instance) {
		
	}

}
