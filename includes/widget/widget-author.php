<?php
namespace Retheme\Widget;

class Widget_Author extends \WP_Widget
{
    public function __construct()
    {
        $args = array(
          'classname' => 'retheme-widget-author',
        );

        parent::__construct('retheme_widget_author', 'Webforia - Author', $args);

    }


    public function widget($args, $instance)
    {

        // outputs the content of the widget
		if (!isset($args['widget_id'])) {
			$args['widget_id'] = $this->id;
		}

		// widget ID with prefix for use in ACF API functions
        $widget_id = 'widget_' . $args['widget_id'];

        // User ID
        $user_id = rt_get_field('user_id', $widget_id);
        

        echo $args['before_widget'];


        if (get_field('display') == 'username') {
            $author_name = get_the_author_meta('user_nicename', $user_id);
        } else {
            $author_name = get_the_author_meta('first_name', $user_id) . ' ' . get_the_author_meta('last_name', $user_id);
        }

        $data['author_gravatar']   = get_avatar($user_id, 80);
        $data['author_desc']       = get_the_author_meta('description', $user_id);
        $data['author_job']        = rt_get_field('job', 'user_'.$user_id);
        $data['author_instagram']  = rt_get_field('instagram',"user_{$user_id}");
        $data['author_linkedin']   = rt_get_field('linkedin', "user_{$user_id}");
        $data['author_fb']         = rt_get_field('facebook', "user_{$user_id}");
        $data['author_tw']         = rt_get_field('twitter', "user_{$user_id}");
        $data['author_blog']       = get_the_author_meta('blog_title', $user_id);
        $data['author_url']        = get_the_author_meta('url', $user_id);
        $data['author_posts']      = get_author_posts_url($user_id);
        $data['author_website']    = get_the_author_meta($user_id, 'url');
        $data['author_avatar']     = rt_get_field('avatar', "user_{$user_id}");
        $data['author_background'] = wp_get_attachment_image_src(rt_get_field('background', "user_{$user_id}"), 'medium')[0];

        wex_get_template_part('widget/author', $data);

        echo $args['after_widget'];
    }


    public function form($instance)
    {
      
    }

    public function update($new_instance, $old_instance)
    {
       
    }
}
