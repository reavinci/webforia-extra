<?php
namespace Retheme;

class Widget_Init
{

    public function __construct()
    {

        add_action('widgets_init', array($this, 'register_widget_element'));

    }

    public function register_widget_element()
    {

        include dirname(__FILE__) . '/widget-author.php';
        include dirname(__FILE__) . '/widget-posts.php';
        include dirname(__FILE__) . '/widget-posts-tabs.php';
        include dirname(__FILE__) . '/widget-social.php';


        register_widget('Retheme\Widget\Widget_Author');
        register_widget('Retheme\Widget\Widget_Posts');
        register_widget('Retheme\Widget\Widget_Post_Tabs');
        register_widget('Retheme\Widget\Widget_Sosmed');


    }

}

new Widget_Init;
