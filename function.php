<?php
/*=================================================;
/* GET TEMPLATE
/*================================================= */
function wex_get_template_part($template, $data = '')
{
    $loader = new WEX_Template_Loader;

    $loader->set_template_data($data)
        ->get_template_part($template);

}
