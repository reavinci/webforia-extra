<?php

/**
 *
 */
use Retheme\Customizer_Base;

class WEX_Final
{

    public function __construct()
    {

        add_action('plugins_loaded', [$this, 'load_plugin_textdomain']);
        
    }

    public function load_plugin_textdomain()
    {
        load_plugin_textdomain('webforia_extra', false, WEBFORIA_EXTRA_TEMPLATE . '/languages/');
    }

}

new WEX_Final();
