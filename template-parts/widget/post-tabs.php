<div class="rt-tab rt-tab--full-nav js-tab" data-animatein="transition.fadeIn" data-duration='300'>
    <div class="rt-tab__nav">
        <?php if($data->popular): ?>
        <li>
            <h5 href="#popular" class="rt-tab__title is-active"><?php _e('Popular', 'rt_domain') ?></h5>
        </li>
        <?php endif ?>

        <?php if($data->most_comment): ?>
        <li>
            <h5 href="#comment" class="rt-tab__title"><?php _e('Most Comments', 'rt_domain') ?></h5>
        </li>
        <?php endif ?>

        <?php if($data->lastest): ?>
        <li>
            <h5 href="#lastest" class="rt-tab__title"><?php _e('Lastest', 'rt_domain') ?></h5>
        </li>
        <?php endif ?>
    </div>


    <div class="rt-tab__body">

      <?php if($data->popular):?>
        <div id="popular" class="rt-tab__item is-active">
              <div class="rt-post-widget">
                   <?php
                   $the_query = new \WP_Query($data->query_popular);

                   if ($the_query->have_posts()){

                    while ($the_query->have_posts()): $the_query->the_post();

                        ?>
                         <div class="rt-post rt-post--list-small">

                            <div class="rt-post__thumbnail rt-img rt-img--full">
                                <?php rt_the_post_thumbnail(apply_filters('rt_widget_image_post_size', 'thumbnail' ))?>
                            </div>

                            <div class="rt-post__body">

                            <h6 class="rt-post__title">
                                <a href="<?php the_permalink()?>"><?php the_title()?></a>
                            </h6>

                            <div class="rt-post__meta">

                                    <span class="rt-post__meta-item post_view">
                                        <i class="fa fa-eye" aria-hidden="true"></i><?php echo get_post_meta(the_ID(), 'wp_post_views_count') ?>
                                    </span>

                            </div>

                            </div>
                        </div>

                        <?php

                    endwhile;

                    wp_reset_postdata(); 

                   }else{
                        _e('No Result', 'rt_domain');
                   }
                
                    ?>
              </div>
        </div>
      <?php endif ?>


        <?php if($data->most_comment):?>

        <div id="comment" class="rt-tab__item">
              <div class="rt-post-widget">
              <?php
                   $the_query = new \WP_Query($data->query_most_comment);

                   if ($the_query->have_posts()){

                    while ($the_query->have_posts()): $the_query->the_post();

                        ?>
                         <div class="rt-post rt-post--list-small">

                            <div class="rt-post__thumbnail rt-img rt-img--full">
                               <?php rt_the_post_thumbnail(apply_filters('rt_widget_image_post_size', 'thumbnail'))?>

                            </div>

                            <div class="rt-post__body">

                            <h6 class="rt-post__title">
                                <a href="<?php the_permalink()?>"><?php the_title()?></a>
                            </h6>

                            <div class="rt-post__meta">

                                    <span class="rt-post__meta-item post_comment">
                                        <i class="fa fa-comment"></i><?php echo get_comments_number() ?>
                                    </span>

                            </div>

                            </div>
                        </div>

                        <?php

                    endwhile;

                    wp_reset_postdata(); 

                   }else{
                        _e('No Result', 'rt_domain');
                   }

                    ?>
              </div>
        </div>
        
        <?php endif; ?>


        <?php if($data->lastest):?>
        <div id="lastest" class="rt-tab__item">
              <div class="rt-post-widget">
              <?php
                   $the_query = new \WP_Query($data->query_lastest);

                   if ($the_query->have_posts()){

                    while ($the_query->have_posts()): $the_query->the_post();

                        ?>
                         <div class="rt-post rt-post--list-small">

                        <div class="rt-post__thumbnail rt-img rt-img--full">
                            <?php rt_the_post_thumbnail(apply_filters('rt_widget_image_post_size', 'thumbnail' ))?>
                        </div>

                        <div class="rt-post__body">

                        <h6 class="rt-post__title">
                            <a href="<?php the_permalink()?>"><?php the_title()?></a>
                        </h6>

                        <div class="rt-post__meta">

                                <span class="rt-post__meta-item">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i><?php echo get_the_date() ?>
                                </span>


                        </div>

                        </div>
                    </div>

                        <?php

                    endwhile;
                    wp_reset_postdata(); 

                   }else{
                        _e('No Result', 'rt_domain');
                   }

                    ?>
              </div>
        </div>
        <?php endif; ?>

    </div>
</div>
