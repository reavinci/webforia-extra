 <div class="rt-post rt-post--list-small">

	<div class="rt-post__thumbnail rt-img rt-img--full">
	 	<?php rt_the_post_thumbnail(apply_filters('rt_widget_image_post_size', 'thumbnail' ))?>
	</div>

	<div class="rt-post__body">

	  <h5 class="rt-post__title">
	      <a href="<?php the_permalink()?>"><?php the_title()?></a>
	  </h5>

	  <div class="rt-post__meta">
	  	<span class="rt-post__meta-item "><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo get_the_date() ?></span>
	  </div>
	 </div>

</div>
