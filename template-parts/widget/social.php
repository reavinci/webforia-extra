<div class="rt-widget__body">

  <div class="<?php echo $data->class; ?>">

    <?php if(is_array(rt_option('social_item'))): ?>
      <?php foreach (rt_option('social_item') as $key => $value): ?>

        <a href="<?php echo $value['link_url'] ?>" class="rt-socmed__item <?php echo $value['link_text'] ?>">
              <i class="<?php echo 'fa fa-'.$value['link_text'] ?>"></i>
              <?php if($data->layout === 'vertical-list'): ?>
              	<span><?php echo $value['link_text'] ?></span>
              <?php endif; ?>
        </a>

      <?php endforeach; ?>
    <?php endif ?>

  </div>

</div>